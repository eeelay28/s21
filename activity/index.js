console.log("Hello world");

// function which is able to receive a single argument and add the the input at the end of the users array

let inputParameter=prompt("input a fruit");

let dataArray = ["Apple","Orange","Kiwi","Lychee"];
console.log(dataArray);

function receiveArgument(inputtedParameter){
  dataArray[dataArray.length++]=inputtedParameter;
}

receiveArgument(inputParameter);
console.log(dataArray);

// create a function which is able to receive an index as a single argument return the item accessed by its index.

function getDataByIndex(index) {
  return dataArray[index];
}
let dataFound = getDataByIndex(3);
console.log(dataFound);

// create a function which is able to delete the last item in the array and return the deleted item.

function getLastElementIndex(lastElement) {
  return getLastElementIndex[lastElement];
}
let deleteLastElementIndex = dataArray.length-1;

console.log(dataArray[deleteLastElementIndex]);

dataArray.length--;

console.log(dataArray);
getLastElementIndex(deleteLastElementIndex);


// create a function which is able to update a specific item in the array by its index

function updateSpecificItem(index,updatedData){
  dataArray[index] = updatedData;
}

updateSpecificItem(2,"Mango");
console.log(dataArray);

// delete all the items in the array

function deleteArray(){
  dataArray=[];
}

deleteArray();
console.log(dataArray);

// check if the array is empty

function checkArrayLength(){
  if ( dataArray.length > 0 ){
    return false;
  } else {
    return true;
  }
};

console.log(checkArrayLength());
